# this will start and deploy a kubernetes cluster and open the dashboard
#install minikube using brew
brew install minikube
#starts kubernetes cluster
minikube start
#outputs kubectl client and server version
kubectl version
#view the nodes available in the cluster
kubectl get nodes
# lists pods running in cluster (-A will return pods running in all namespacesviavan)
kubectl get pods -A
#opens dashboard
minikube dashboard

