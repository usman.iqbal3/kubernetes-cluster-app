# Kubernetes-cluster-app

## What is Helm?
Helm is an application package manager for Kubernetes, which coordinates the download, installation, and deployment of apps.


This guide will demonstrate the creation of a basic Helm chart to deploy a nginx web server.

- Follow this guide to install homebrew:
https://dwpdigital.atlassian.net/wiki/spaces/RET/pages/132443177515/Configure+your+DWP+Mac+for+DevOps+Tools  

# Install Minikube
- `brew install minikube`

## Start Kubernetes cluster
- `minikube start`

### Manage your cluster
- `minikube pause`
- `minikube unpause`
- `minikube stop`
- `minikube delete --all`

# Install Helm
- `brew install helm`

## Check if you have all prerequisites installed...
- `which helm` - should list the path 
- `minikube status` -    
output:  
***host: Running***  
***kubelet: Running***  
***apiserver: Running***  
***kubeconfig: Configured***

# Building the helm chart
- `helm create 'chartname'`
- `ls 'chartname'` - should display Chart.yaml/ charts/ templates/  values.yaml

## Quick examination of the files
- Chart.yml and values.yml

### Editing the values.yml
- for this project we will be making a few changes to the values.yml file, these are:

- pull policy - change to `Always`. 
- nameOverride: "blueberry-app" - **you can rename this to your liking**. 
- fullnameOverride: "blueberry" -  **you can rename this to your liking**. 
- name - "bluebomb" -  **you can rename this to your liking**. 
- service:  
type: change from ClusterIp to `NodePort`
- ingress:  
enabled: keep it `false`
- resources: Uncomment:    
limits:  
     cpu: 100m  
     memory: 128Mi  
   requests:  
     cpu: 100m  
     memory: 128Mi  


# Deploy Helm

Now that we've made the neccesary changes to the helm configurations. To deploy:
- helm install my-blueberry buildachart/ --values buildachart/values.yaml

### you will get instructions on your ternimanl similar to this:
Get the application URL by running these commands:
- export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{. spec.ports[0].nodePort}" services blueberry)  
- export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")  
- echo http://$NODE_IP:$NODE_PORT
    - http://192.168.49.2:30409 - make a note of the number after the semi-colon.
- run `kubectl get pods` to get the active pods within your cluster. Make a note of the pod name.
- run `kubectl port-forward blueberry-644484fd8-pp8kb 8080:80`. 

Now you have deployed th helm chart. To open it in your browser:
- type: `http://127.0.0.1:8080` in your browser. 















